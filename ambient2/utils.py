import os
import h5py
import obspy
import numpy as np
import warnings
import matplotlib.pyplot as plt
import pyasdf

from glob import glob
from configobj import ConfigObj
from obspy import read_inventory
from scipy.fftpack import fft,ifft
from obspy.signal.util import smooth
from scipy.ndimage import uniform_filter1d
from scipy.signal import correlate
from obspy.geodetics.base import gps2dist_azimuth
from obspy.geodetics.base import kilometer2degrees

def read_param_dict(inparam_file):
    params = ConfigObj(inparam_file).dict()
    return params

def read_st_from_asdf(filename):
    st = obspy.Stream()
    inv = obspy.Inventory()
    ds = pyasdf.ASDFDataSet(filename,mpi=False,mode='r')
    for waveform in ds.waveforms:
        inv_ = waveform.StationXML
        inv += inv_

        for net_ in inv_:
            net_code = net_.code

            for sta_ in net_:
                sta_code = sta_.code

                for chn_ in sta_:
                    chn_code = chn_.code
                    wave_tag = '{}_{}_{}'.format(net_code.lower(),
                                                 sta_code.lower(),
                                                 chn_code.lower())
                    st += waveform[wave_tag]
    return st, inv

def nextpow2(x):
    res = np.ceil(np.log2(x))
    return res.astype('int')

def write_st_to_asdf(st,inventory,filename):
    ds = pyasdf.ASDFDataSet(filename,mpi=False,mode='w')
    for tr in st:
        tag = '{}_{}_{}'.format(tr.stats.network.lower(),
                                tr.stats.station.lower(),
                                tr.stats.channel.lower())
        ds.add_waveforms(tr,tag)
        inv_ = inventory.select(station=tr.stats.station)
        ds.add_stationxml(inv_)

def clip_on_max(st,sigma_threshold=3):

    for tr in st:
        std = tr.std()
        bad_pos = np.where(tr.data > (sigma_threshold*std))
        bad_neg = np.where(tr.data < (-sigma_threshold*std))
        tr.data[bad_pos] = sigma_threshold*std
        tr.data[bad_neg] = -sigma_threshold*std
    return st

def taper4(f1,f2,f3,f4,dom,nk,npow,sf):
    s = np.zeros(nk)
    for i in range(0,nk):
        f = i*dom
        if(f < f1):
            continue
        elif(f < f2):
            d1 = np.pi/(f2-f1)
            ss = 1.0
            for j in range(0,npow):
                ss = ss*(1.0-np.cos(d1*(f1-f)))/2.0
            s[i] = ss
        elif(f<f3):
            s[i] = 1.
        elif(f<f4):
            d2 = np.pi/(f4-f3)
            ss = 1.0
            for j in range(0,npow):
                ss = ss*(1.0+np.cos(d2*(f3-f)))/2.0
            s[i] = ss
    for i in range(0,nk):
        sf[i] = sf[i] * s[i]

    #sf = sf*s
    return sf

def whiten(s,pw,samprate,flim=None):

    nfft = 2**(nextpow2(2*len(s)))
    fw = np.zeros(nfft,dtype=complex)
    smw = 10
    cn = len(s)

    if flim != None:
        freqs = samprate/2*np.linspace(0,1,nfft/2+1)
        lowindp = np.where(freqs<flim[0])[0]
        lowindp = lowindp[-1] #take largest value
        highindp = np.where(freqs>flim[1])[0]
        highindp = highindp[0] #take smallest value

        lowindn = nfft - highindp+2
        highindn = nfft - lowindp+2
        frq1 = np.arange(lowindp,highindp)
        frq2 = np.arange(lowindn,highindn)
        frq = np.hstack((frq1,frq2))

    else:
        frq = np.arange(0,nfft)

    fs = np.fft.fft(s,nfft)
    fs2 = np.abs(fs)
    fs2 = uniform_filter1d(fs2,smw)
    fw[frq] = fs[frq]
    nzt = np.where(np.abs(fw) != 0)[0] #find NON zero terms
    fw = fw / np.abs(fs2)**pw
    dum = np.fft.ifft(fw)
    w = dum[0:cn]

    return w.real

def whiten_simultaneous(sigz,sign,sige,samprate,freqmin,freqmax):

    print("using simultaneous whitening")

    #nfft = 2**(nextpow2(4*len(sigz)))
    nfft = 2**(nextpow2(2*len(sigz)))
    dt = 1./samprate
    Tmax = 1./freqmin
    Tmin = 1./freqmax
    fb2 = 1./(Tmax)
    fb1 = 1./(Tmax*1.1)
    fe2 = 1./(Tmin)
    fe1 = 1./(Tmin*0.9)
    f1 = fb1
    f2 = fb2
    f3 = fe2
    f4 = fe1

    #take fourier transform
    zfd = np.fft.fft(sigz,nfft)
    nfd = np.fft.fft(sign,nfft)
    efd = np.fft.fft(sige,nfft)

    #kill half of the spectra
    nk = int(nfft/2. + 1)
    zfd[nk:] = 0. + 0j
    nfd[nk:] = 0. + 0j
    efd[nk:] = 0. + 0j

    #get phase and amplitude spectra
    zamp = np.abs(zfd)
    namp = np.abs(nfd)
    eamp = np.abs(efd)
    zphs = np.angle(zfd)
    nphs = np.angle(nfd)
    ephs = np.angle(efd)

    #taper
    dom = 1./dt/(nfft-1)
    npow = 1
    zamp = taper4(f1,f2,f3,f4,dom,nk,npow,zamp)
    namp = taper4(f1,f2,f3,f4,dom,nk,npow,namp)
    eamp = taper4(f1,f2,f3,f4,dom,nk,npow,eamp)

    df = 1.0/dt/(nfft-1)
    n = nfft/2.+1
    nb = int(fb1/df)
    ne = int(fe1/df)

    #find average spectrum of three components
    avg_spec = np.zeros(len(zamp))
    avg_spec_sm = np.zeros(len(zamp))
    for i in range(nb,ne):
        avg_spec[i] = (zamp[i]+namp[i]+eamp[i])/3.0

    #smooth spectrum
    m = int(64./dt)
    for i in range(nb,ne):
        am=0.0
        lag=0
        for n in range(-m,m):
            if(((i+n)>nb) and ((i+n)<ne)):
                am=am+avg_spec[i+n]
                lag += 1
        avg_spec_sm[i] = am/lag

    #divide by smoothed average spectrum
    for i in range(nb,ne):
        zamp[i] = zamp[i]/avg_spec_sm[i]
        namp[i] = namp[i]/avg_spec_sm[i]
        eamp[i] = eamp[i]/avg_spec_sm[i]

    #convert back to time domain
    n = len(zfd)
    zsig_fd = np.zeros(n,dtype=complex)
    nsig_fd = np.zeros(n,dtype=complex)
    esig_fd = np.zeros(n,dtype=complex)
    for i in range(0,n):
        zsig_fd[i] = (zamp[i]*np.cos(zphs[i]))+ (zamp[i]*np.sin(zphs[i]))*1j
        nsig_fd[i] = (namp[i]*np.cos(nphs[i]))+ (namp[i]*np.sin(nphs[i]))*1j
        esig_fd[i] = (eamp[i]*np.cos(ephs[i]))+ (eamp[i]*np.sin(ephs[i]))*1j

    sigz_whitened = np.fft.ifft(zsig_fd)
    sign_whitened = np.fft.ifft(nsig_fd)
    sige_whitened = np.fft.ifft(esig_fd)

    npts = len(sigz) #length of input seismogram
    sigz_out = np.zeros(npts)
    sign_out = np.zeros(npts)
    sige_out = np.zeros(npts)
    for i in range(0,npts):
       sigz_out[i] = (2.0*sigz_whitened[i].real)
       sign_out[i] = (2.0*sign_whitened[i].real)
       sige_out[i] = (2.0*sige_whitened[i].real)

    return sigz_out,sign_out,sige_out


def whiten_three_component(sigz,sign,sige,pw,samprate,flim=None):

    assert(len(sigz)==len(sign)==len(sige))

    nfft = 2**(nextpow2(2*len(sigz)))
    fw_z = np.zeros(nfft,dtype=complex)
    fw_n = np.zeros(nfft,dtype=complex)
    fw_e = np.zeros(nfft,dtype=complex)
    smw = 10
    cn = len(sigz)

    if flim != None:
        freqs = samprate/2*np.linspace(0,1,nfft/2+1)
        lowindp = np.where(freqs<flim[0])[0]
        lowindp = lowindp[-1] #take largest value
        highindp = np.where(freqs>flim[1])[0]
        highindp = highindp[0] #take smallest value

        lowindn = nfft - highindp+2
        highindn = nfft - lowindp+2
        frq1 = np.arange(lowindp,highindp)
        frq2 = np.arange(lowindn,highindn)
        frq = np.hstack((frq1,frq2))

    else:
        frq = np.arange(0,nfft)

    fs_z = np.fft.fft(sigz,nfft)
    fs_n = np.fft.fft(sign,nfft)
    fs_e = np.fft.fft(sige,nfft)

    fs2_z = np.abs(fs_z)
    fs2_n = np.abs(fs_n)
    fs2_e = np.abs(fs_e)

    fs2_z = uniform_filter1d(fs2_z,smw)
    fs2_n = uniform_filter1d(fs2_n,smw)
    fs2_e = uniform_filter1d(fs2_e,smw)

    plt.plot(np.abs(fs2_z))
    plt.plot(np.abs(fs2_n))
    plt.plot(np.abs(fs2_e))
    plt.show()

    fw_z[frq] = fs_z[frq]
    fw_n[frq] = fs_n[frq]
    fw_e[frq] = fs_e[frq]

    #nztz = np.where(np.abs(fw_z) != 0)[0] #find NON zero terms
    #nztn = np.where(np.abs(fw_n) != 0)[0] #find NON zero terms
    #nzte = np.where(np.abs(fw_e) != 0)[0] #find NON zero terms

    #get average of smoothed spectra
    avg_spec = (fs2_z+fs2_n+fs2_e)/3.

    fw_z = fw_z / np.abs(avg_spec)**pw
    fw_n = fw_n / np.abs(avg_spec)**pw
    fw_e = fw_e / np.abs(avg_spec)**pw


    sigz_dum = np.fft.ifft(fw_z)
    sign_dum = np.fft.ifft(fw_n)
    sige_dum = np.fft.ifft(fw_e)

    z = sigz_dum[0:cn]
    n = sign_dum[0:cn]
    e = sige_dum[0:cn]

    return z.real,n.real,e.real

def whiten_stream(st, params, mode='single', flim=None):

    pw = float(params['pw'])
    st_white = obspy.Stream()
    freqmin = 1./float(params['maximum_period'])
    freqmax = 1./float(params['minimum_period'])

    if mode == 'single':
        for tr in st:
            tr.data = whiten(tr.data,pw,tr.stats.sampling_rate)
            st_white += tr

    elif mode == 'multi':
        stations = []
        for tr in st:
            stations.append(tr.stats.station)

        unique_stations = np.unique(stations)
        for station in unique_stations:
            st_here = st.select(station=station)

            try:
                trz = st_here.select(channel='**Z')[0]
                trn = st_here.select(channel='**N')[0]
                tre = st_here.select(channel='**E')[0]
            except IndexError:
                warnings.warn('attempted 3 component norm. but couldnt find all three of Z N and E')
                continue

            samprate = trz.stats.sampling_rate
            #zdata,ndata,edata = whiten_three_component(trz.data,trn.data,tre.data,pw,samprate)
            zdata,ndata,edata = whiten_simultaneous(trz.data,trn.data,tre.data,samprate,freqmin,freqmax)
            trz.data = zdata
            trn.data = ndata
            tre.data = edata

            st_white += trz
            st_white += trn
            st_white += tre

    return st_white

def one_bit_filter(st):
    '''
    takes in an obspy stream and 1-bit filters the data
    '''
    st.normalize()
    st.detrend()
    for tr in st:
        tr.data = np.sign(tr.data)

    return st

def running_average_norm(st,window_s,samprate,mode='single'):
    '''
    Performs a running average time domain normalization, as
    described in Bensen et al. (2007)

    st: obspy stream
    window_s: half duration of averaging window in s
    mode: 'single' if you want to perform the average on each
          trace individually.
          'multi' if you want to normalize based on the maximum
          envelope of a three component stream. (see Lin et al. 2014)
    '''

    npts = int(window_s * samprate)
    st_norm = obspy.Stream()

    if mode =='single':

        for tr in st:
            data = tr.copy().data
            data = np.abs(data)
            data_s = smooth(data,npts)
            tr.data /= data_s
            st_norm += tr

    elif mode == 'multi':
    #assumes Z,N,E components all available

        stations = []
        for tr in st:
            stations.append(tr.stats.station)

        unique_stations = np.unique(stations)
        for station in unique_stations:
            st_here = st.select(station=station)

            try:
                trz = st_here.select(channel='**Z')[0]
                trn = st_here.select(channel='**N')[0]
                tre = st_here.select(channel='**E')[0]
            except IndexError:
                warnings.warn('attempted 3 component norm. but couldnt find all three of Z N and E')
                continue

            dt = trz.stats.delta
            N = int(window_s/2./dt+0.5)
            sigz_norm = np.zeros(trz.stats.npts)
            sign_norm = np.zeros(trn.stats.npts)
            sige_norm = np.zeros(tre.stats.npts)

            for i in range(0,trz.stats.npts):

                i_end = i+N
                i_start = min(0,i)
                i_start = max(i_start,i_end-(2*N))
                if i_end > st_here[0].stats.npts:
                    i_end = st_here[0].stats.npts + 1
                lag = i_end - i_start + 1

                z_sum = np.sum(np.abs(trz.data[i_start:i_end]))
                n_sum = np.sum(np.abs(trn.data[i_start:i_end]))
                e_sum = np.sum(np.abs(tre.data[i_start:i_end]))
                max_sum = max([z_sum,n_sum,e_sum])

                sigz_norm[i] = trz.data[i]*lag/max_sum
                sign_norm[i] = trn.data[i]*lag/max_sum
                sige_norm[i] = tre.data[i]*lag/max_sum

            trz.data = sigz_norm
            trn.data = sign_norm
            tre.data = sige_norm

            st_norm += trz
            st_norm += trn
            st_norm += tre

    return st_norm

def xcorr_day(st1,st2,params,day_start,cross_component,i_day,debug=True):
    '''
    takes two day long streams (can be of different components),
    and computes cross correlations all of the possible combinations of
    station pairs.
    '''

    #in case there are duplicate traces in stream
    st1.merge(method=1)
    st2.merge(method=1)

    print('*****')
    print(st1)
    print(st2)
    print('*****')

    if debug:
        print(':::xcorr_day:::  working on day {}'.format(i_day))
        print('stream 1',st1)
        print('stream 2',st2)

    #basic parameters
    samprate = float(params['samprate'])
    max_lag_pts = int(float(params['max_lag_s']) * samprate)
    npts_corr = 2*max_lag_pts + 1

    if params['xcorr_format'] == 'hdf5':
        day_string = 'day_{}_{:02d}_{:02d}'.format(day_start.year,day_start.month,day_start.day)
        xcorr_ds = h5py.File(params['run_name']+'/day_xcorrs/{}_{}.h5'.format(
                             day_string,cross_component),'w')

    #processing each day stream
    st1_ = st1.copy()
    st2_ = st2.copy()
    st1_.sort()
    st2_.sort()

    #clip outliers
    #st1_ = clip_on_max(st1_,sigma_threshold=3)
    #st2_ = clip_on_max(st2_,sigma_threshold=3)

    #taper
    r1 = 50.0
    r = r1/((1./samprate)*0.5*len(st1[0].data))
    st1_.taper(r)
    st2_.taper(r)

    #filter? right now it filters before saving the day data
    max_per = float(params['maximum_period'])
    min_per = float(params['minimum_period'])
    st1_.filter('bandpass',
                 freqmin = 1./max_per,
                 freqmax = 1./min_per,
                 corners = 4,
                 zerophase = True)
    st2_.filter('bandpass',
                 freqmin = 1./max_per,
                 freqmax = 1./min_per,
                 corners = 4,
                 zerophase = True)

    #temporal normalization?
    if params['time_weighting'] == 'running_average':
        running_avg_s = float(params['running_avg_s'])
        st1_ = running_average_norm(st1_,running_avg_s,samprate,'single')
        st2_ = running_average_norm(st2_,running_avg_s,samprate,'single')
    elif params['time_weighting'] == '1bit':
        st1_ = one_bit_filter(st1_)
        st2_ = one_bit_filter(st2_)

    if params['freq_weighting'] == 'whiten':
        st1_ = whiten_stream(st1_,params,mode='single')
        st2_ = whiten_stream(st2_,params,mode='single')

    #this taper seems to diminish zero lag energy in cross correlations
    st1_.taper(0.01)
    st2_.taper(0.01)

    #for trimming into shorter segments (if desired)
    xcorr_seg_dur_min = float(params['xcorr_seg_dur_min'])
    s_per_day = 60.*60.*24.
    s_per_seg = xcorr_seg_dur_min * 60.0
    seis_npts = int(s_per_day * samprate)
    seg_npts = int(xcorr_seg_dur_min * samprate * 60.)
    seg_starts = np.arange(0, int(s_per_day - s_per_seg - 1), int(s_per_seg/2))

    tday = st1[0].stats.starttime + 120. #for naming
    #make dictionary with all station locations
    stations_list = []
    stations_dict = {}

    if params['format'].lower() == 'asdf':
        ds = pyasdf.ASDFDataSet(params['run_name']+'/day_data/day_{}_{:02d}_{:02d}.h5'.format(tday.year,tday.month,tday.day),mpi=False,mode='r')
        inventory = obspy.Inventory()

        for waveform in ds.waveforms:
            inventory += waveform.StationXML

    elif params['format'].lower() == 'miniseed' or params['format'].lower() == 'mseed':
        try:
            inventory = obspy.read_inventory(params['run_name']+'/stations/stations_{}_{:02d}_{:02d}.xml'.format(tday.year,tday.month,tday.day))
        except IOError:
            inventory = obspy.read_inventory(params['run_name']+'/stations/stations_day_{:03d}.xml'.format(i_day))

    for network in inventory:
        for station in network:
            stations_list.append(station.code)
            dict_here = {'longitude':station.longitude,
                         'latitude':station.latitude,
                         'elevation':station.elevation}
            stations_dict[station.code] = dict_here

    #start loop between all possible pairs
    for i in range(0,len(st1_)):
        for j in range(i,len(st2_)):

            stlo = inventory.select(station=st1_[i].stats.station)[0][0].longitude
            stla = inventory.select(station=st1_[i].stats.station)[0][0].latitude
            stel = inventory.select(station=st1_[i].stats.station)[0][0].elevation
            evlo = inventory.select(station=st2_[j].stats.station)[0][0].longitude
            evla = inventory.select(station=st2_[j].stats.station)[0][0].latitude
            evel = inventory.select(station=st2_[j].stats.station)[0][0].elevation

            #make directory structure
            path_name = '{}.{}_{}.{}'.format(st1_[i].stats.network,
                                             st1_[i].stats.station,
                                             st2_[j].stats.network,
                                             st2_[j].stats.station)

            print(i,j)
            print('working on path {}: stlo,stla,evlo,evla: {}, {}, {}, {}'.format(path_name,stlo,stla,evlo,evla))

            if params['xcorr_format'].lower() == 'sac':
                xcorr_path_name = params['run_name']+'/day_xcorrs/day_{}_{:02d}_{:02d}/{}/{}'.format(tday.year,
                    tday.month,tday.day,cross_component,path_name)

                if not os.path.exists(xcorr_path_name):
                    os.makedirs(xcorr_path_name)

            #get gps info
            distaz = gps2dist_azimuth(stla,stlo,evla,evlo)
            dist_m = distaz[0]
            dist = dist_m / 1000.
            az = distaz[1]
            baz = distaz[2]
            gcarc = kilometer2degrees(dist)

            #initiate trace
            xcorr_trace = obspy.Trace()
            xcorr_trace.stats.sampling_rate = samprate
            xcorr_trace.stats.sac = {}
            xcorr_trace.stats.sac = {'stlo':stlo,
                                     'stla':stla,
                                     'stel':stel,
                                     'evlo':evlo,
                                     'evla':evla,
                                     'evel':evel,
                                     'dist':dist,
                                     'gcarc':gcarc,
                                     'az':az,
                                     'baz':baz}

            xcorr_sum = 0

            #start loop over all segments
            #for i_seg in trange(0,len(seg_starts)):
            for i_seg in range(0,len(seg_starts)):

                t1_seg = day_start + seg_starts[i_seg]
                t2_seg = day_start + seg_starts[i_seg] + xcorr_seg_dur_min*60.

                st1_seg = st1_[i].copy().trim(t1_seg, t2_seg)
                st2_seg = st2_[j].copy().trim(t1_seg, t2_seg)

                tr1 = st1_seg.copy()
                tr2 = st2_seg.copy()

                Nt = tr1.stats.npts

                #actual cross correlation
                xcorr_here = correlate(tr2.data,tr1.data,mode='full')

                #only save up to max_lag_s
                tcorr = np.arange(-Nt+1 , Nt)
                dN = np.where(np.abs(tcorr) < max_lag_pts+1)[0]
                xcorr_here = xcorr_here[dN]

                #normalize segment?
                #xcorr_here /= np.max(xcorr_here)

                #add segment xcorrs
                xcorr_sum += xcorr_here

            #divide by number of segments
            xcorr_sum /= len(seg_starts)
            xcorr_trace.data = xcorr_sum

            #write trace
            if params['xcorr_format'] == 'sac':
                xcorr_trace.write(xcorr_path_name+'/'+path_name+'.sac',format='SAC')
            elif params['xcorr_format'] == 'hdf5':
                print("SHOULD BE HERE *********\n")
                print(len(xcorr_trace.data))
                ds_path = xcorr_ds.create_dataset(name=path_name, data=xcorr_trace.data)
                ds_path.attrs['stlo'] = stlo
                ds_path.attrs['stla'] = stla
                ds_path.attrs['stel'] = stel
                ds_path.attrs['evlo'] = evlo
                ds_path.attrs['evla'] = evla
                ds_path.attrs['evel'] = evel
                ds_path.attrs['gcarc'] = gcarc
                ds_path.attrs['dist'] = dist
                ds_path.attrs['az'] = az
                ds_path.attrs['baz'] = baz
                ds_path.attrs['samprate'] = samprate

def run_xcorrs(st,params,day_start,i_day,debug=False):

    '''
    This function takes a multi-component stream object containing
    one day of data.It trims the day into multiple segments, cross
    correlates all possible pairs, and stacks.
    '''
    samprate = float(params['samprate'])

    if debug:
        print("from run_xcorrs: ", st)

    #demean and detrend
    st.detrend(type='demean')
    st.detrend()

    #taper
    r1 = 50.0
    r = r1/((1./samprate)*0.5*len(st[0].data))
    st.taper(r)

    #filter
    max_per = float(params['maximum_period'])
    min_per = float(params['minimum_period'])
    st.filter('bandpass',
               freqmin = 1./max_per,
               freqmax = 1./min_per,
               corners = 4,
               zerophase = True)

    #time domain normalization
    if params['time_weighting'] == 'running_average':
        running_avg_s = float(params['running_avg_s'])
        if params['three_component_normalization'] == 'True':
            st = running_average_norm(st.copy(),running_avg_s,samprate,'multi')
        else:
            st = running_average_norm(st.copy(),running_avg_s,samprate,'single')
    elif params['time_weighting'] == '1bit':
        st = one_bit_filter(st.copy())

    #spectral whitening
    #TODO change spectral whitening to work with three component data
    #should divide each component with the average of the smoothed 3 component spectra
    if params['freq_weighting'] == 'whiten':
        if params['three_component_normalization'] == 'True':
            st = whiten_stream(st.copy(),params,mode='multi')
        else:
            st = whiten_stream(st.copy(),params,mode='single')

    st_e = st.copy().select(channel='**E')
    st_n = st.copy().select(channel='**N')
    st_z = st.copy().select(channel='**Z')

    if type(params['cross_components']) == str:
        cross_components = [params['cross_components']]
    else:
        cross_components = params['cross_components']

    #perform the cross correlations
    for cross_component in cross_components:
        print("WORKING ON COMPONENT {}".format(cross_component))
        if cross_component == 'ZZ':
            xcorr_day(st_z,st_z,params,day_start,cross_component=cross_component,i_day=i_day)

        elif cross_component == 'NN':
            xcorr_day(st_n,st_n,params,day_start,cross_component=cross_component,i_day=i_day)

        elif cross_component == 'EE':
            xcorr_day(st_e,st_e,params,day_start,cross_component=cross_component,i_day=i_day)

        elif cross_component == 'ZN':
            xcorr_day(st_z,st_n,params,day_start,cross_component=cross_component,i_day=i_day)

        elif cross_component == 'NZ':
            xcorr_day(st_n,st_z,params,day_start,cross_component=cross_component,i_day=i_day)

        elif cross_component == 'ZE':
            xcorr_day(st_z,st_e,params,day_start,cross_component=cross_component,i_day=i_day)

        elif cross_component == 'EZ':
            xcorr_day(st_e,st_z,params,day_start,cross_component=cross_component,i_day=i_day)

        elif cross_component == 'NE':
            xcorr_day(st_n,st_e,params,day_start,cross_component=cross_component,i_day=i_day)

        elif cross_component == 'EN':
            xcorr_day(st_e,st_n,params,day_start,cross_component=cross_component,i_day=i_day)
