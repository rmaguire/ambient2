import glob
import obspy
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

def bootstrap(data_dir,path_name,cross_component,M,conf_level=0.9,plot=False):

    #sacfile_list = glob.glob(data_dir+'/*/'+path_name+'/*')
    query_path = '{}/*/{}/{}/*'.format(data_dir,cross_component,path_name)
    print(query_path)
    sacfile_list = glob.glob(query_path)
    print(sacfile_list)
    bootstrap_realization = []
    signal_list = []

    N = len(sacfile_list)

    for sacfile in sacfile_list:
        #st += obspy.read(sacfile)
        st = obspy.read(sacfile)
        tr = st[0]
        signal_list.append(tr.copy().data)

    for i in range(0,M):
        rand = np.random.random_integers(0,N-1,N)
        stack = 0
        for j in rand:
            stack += signal_list[j]
        stack /= N
        bootstrap_realization.append(stack)

    A = np.vstack(bootstrap_realization)
    mean = A.mean(0)
    std = A.std(0)
    conf_int = stats.norm.interval(conf_level,loc=mean,scale=std)
    
    if plot:
        time = tr.times()
        time -= np.average(time)
        plt.plot(time,mean)
        plt.show()

    return mean,std,conf_int

def get_stream_all_days(data_dir,path_name,cross_component):

    query_path = '{}/*/{}/{}/*'.format(data_dir,cross_component,path_name)
    sacfile_list = glob.glob(query_path)
    st = obspy.Stream()

    for sacfile in sacfile_list:
        st += obspy.read(sacfile)

    return st
