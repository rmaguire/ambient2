#!/usr/bin/env python
'''
A hack to adapt the obspy client to the non-standard msds fdsn server

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2018
:license:
    None
'''
from obspy.clients.fdsn import Client as StandardClient
from obspy.clients.fdsn.client import CustomRedirectHandler, \
        NoRedirectionHandler, build_url
import urllib.request as urllib_request

class Client(StandardClient):

    # hacking the Basic authentication into the fdsn client
    def _set_opener(self, user, password, digest=False):
        # Only add the authentication handler if required.
        handlers = []
        if user is not None and password is not None:
            # Create an OpenerDirector for HTTP Digest Authentication
            password_mgr = urllib_request.HTTPPasswordMgrWithDefaultRealm()
            password_mgr.add_password(None, self.base_url, user, password)

            handlers.append(urllib_request.HTTPBasicAuthHandler(password_mgr))

        if (user is None and password is None) or self._force_redirect is True:
            # Redirect if no credentials are given or the force_redirect
            # flag is True.
            handlers.append(CustomRedirectHandler())
        else:
            handlers.append(NoRedirectionHandler())

        # Don't install globally to not mess with other codes.
        self._url_opener = urllib_request.build_opener(*handlers)
        if self.debug:
            print('Installed new opener with handlers: {!s}'.format(handlers))

    # MSDS uses query also for authenticated queries
    def _build_url(self, service, resource_type, parameters={}):
        return build_url(self.base_url, service, self.major_versions[service],
                         resource_type, parameters,
                         service_mappings=self._service_mappings)

