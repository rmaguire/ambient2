from __future__ import print_function

def plot_station_xml(xml_file,plot_sta_codes=False,
                     etopo=False,blue_marble=False,
                     plot_rivers=False,
                     plot_interstation_paths=False):
    inventory = read_inventory(xml_file)
    lons = []
    lats = []
    sta_codes = []

    for network in inventory:
        for station in network:
            lons.append(station.longitude)
            lats.append(station.latitude)
            sta_codes.append(station.code)

    min_lon = np.min(lons)
    max_lon = np.max(lons)
    min_lat = np.min(lats)
    max_lat = np.max(lats)

    lon_span = max_lon - min_lon
    lat_span = max_lat - min_lat

    #global map
    if lon_span > 45 or lat_span > 45:
        m = Basemap(projectiion = 'hammer', lon_0 = (lon_min+lon_max)/2.,resolution='c')
        m.drawcoastlines()

    #local map
    else:
        m = Basemap(projection = 'merc',
                    resolution='i',
                    llcrnrlat = min_lat - 2.0,
                    llcrnrlon = min_lon - 2.0,
                    urcrnrlat = max_lat + 2.0,
                    urcrnrlon = max_lon + 2.0)

        if blue_marble:
            m.bluemarble()
        elif etopo:
            m.shadedrelief()

        if plot_rivers:
            m.drawrivers()

        m.drawcountries()
        m.drawcoastlines()
        m.drawstates()

    x,y = m(np.array(lons),np.array(lats))
    m.scatter(x,y, marker = '^', edgecolor='black', facecolor='red', s=100)

    count = 0
    if plot_interstation_paths:
        for i in range(0,len(x)):
            for j in range(i,len(y)):

                pt1 = m(lons[i],lats[i])
                pt2 = m(lons[j],lats[j])
                xs = zip(pt1,pt2)[0]
                yz = zip(pt1,pt2)[1]
                m.plot(xs,yz,c='k',alpha=0.15)

                count += 1

    if plot_sta_codes:
        for i in range(0,len(sta_codes)):
            plt.text(x[i],y[i],sta_codes[i],fontsize=12,color='blue')

    plt.show()

def plot_xcorr(day_corrs_dir,component,station_1,station_2):

    sampling_rate = 2.0 #TODO FIX SO IT READS FROM H5 FILE
    files = glob(day_corrs_dir+'/day_*')
    xcorr_sum = 0
    path_code = '{}_{}'.format(station_1,station_2)
    days_in_stack = 0

    for i in range(0,len(files)):
        ds = h5py.File(files[i])
        try:
            d_here = ds[component][path_code][:]
            xcorr_sum += d_here
            days_in_stack += 1
        except KeyError:
            print('station pair not found on day ', i)

    if type(xcorr_sum) != int:
        fig,ax = plt.subplots(1,figsize=[8,3])
        #xcorr_sum /= np.max(xcorr_sum)
        plt.plot(xcorr_sum)
        plt.title('component = {},days in stack {}'.format(component,days_in_stack))
        plt.show()

    else:
        print('Sorry... didnt find that path')
        print('Available components are ', ds.keys())
