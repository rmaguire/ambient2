from __future__ import print_function
import os
import sys
import time
import obspy
import pyasdf
import warnings
import numpy as np
import matplotlib.pyplot as plt

from mpi4py import MPI
from shutil import copyfile
from obspy import UTCDateTime

if sys.version_info[0] < 3:
    from data import get_waveforms
    from utils import run_xcorrs
    from utils import one_bit_filter
    from utils import read_param_dict
else:
    #from ambient2.ambient2.data import get_waveforms
    #from ambient2.ambient2.utils import run_xcorrs
    #from ambient2.ambient2.utils import one_bit_filter
    #from ambient2.ambient2.utils import read_param_dict
    from ambient2.data import get_waveforms
    from ambient2.utils import run_xcorrs
    from ambient2.utils import one_bit_filter
    from ambient2.utils import read_param_dict

# ===================================================================
#
# --------------------- Main Program --------------------------------
#
# ===================================================================

def main(params_file,day_start,debug=False):

    #-----------------------------------------------------------------------
    #Initialize MPI
    #-----------------------------------------------------------------------
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    if rank == 0:
        #-----------------------------------------------------------------------
        #Read run parameters
        #-----------------------------------------------------------------------

        params = read_param_dict(params_file)
        if not os.path.exists(params['run_name']): 
            os.mkdir(params['run_name'])
            os.mkdir(params['run_name']+'/day_data')
            os.mkdir(params['run_name']+'/day_xcorrs')
            os.mkdir(params['run_name']+'/stations')

        #copy input parameters file to run directory
        copyfile(params_file,params['run_name']+'/'+params_file)

        s_per_day = 60.*60.*24.
        samprate = float(params['samprate'])
        starttime = UTCDateTime(params['startdate'])
        endtime   = UTCDateTime(params['enddate'])
        n_days = (endtime - starttime) / s_per_day
        n_days = int(n_days)
        print("n_days: {}".format(n_days))

        min_per = float(params['minimum_period'])
        max_per = float(params['maximum_period'])
 
        print('-'*77)
        print('starting a run for', int(n_days), 'days')
        print('-'*77)

        #splits = n_days - 1
        splits = n_days
    else:
        n_days = None
        splits = None
        params = None

    #-----------------------------------------------------------------------
    #Loop through all days 
    #-----------------------------------------------------------------------

    n_days = comm.bcast(n_days,root=0)
    splits = comm.bcast(splits,root=0)
    params = comm.bcast(params,root=0)

    print('MPI INFO-------------------------------------------------------------')
    print('n_days: {}'.format(n_days))
    print('splits: {}'.format(splits))
    print('size: {}'.format(size))
    print('rank: {}'.format(rank))
    print('---------------------------------------------------------------------')

    for i_day in range(rank,splits,size):

        print('working on day', i_day)
        starttime = UTCDateTime(params['startdate'])
        seg_dur_s = 60.*60.*24. #24 hours
        s_per_day = 60.*60.*24. #24 hours
        day_start = starttime + s_per_day*i_day

        #-----------------------------------------------------------------------
        #Determine which mode to run the code in 
        #-----------------------------------------------------------------------
        if params['mode'] == 'download':
            download = True
            xcorr = False
        elif params['mode'] == 'xcorr':
            download = False
            xcorr = True
        elif params['mode'] == 'both':
            download = True
            xcorr = True
        else:
            raise ValueError('mode',params['mode'],'is not valid')

        #-----------------------------------------------------------------------
        #remove response?
        #-----------------------------------------------------------------------
        if params['remove_response'] == 'True':
            remove_response = True
            pre_filt = [0.001, 0.005, 45, 50] #pre filt for inst. correction
        elif params['remove_response'] == 'False':
            remove_response = False
        else:
            raise ValueError('remove_response must be "True" or "False"')

        #to facilitate restarts, it will check if the day data and/or day xcorrs exist
        if os.path.exists(params['run_name']+'/day_data/day_{}_{:02d}_{:02d}.h5'.format(day_start.year,
            day_start.month,day_start.day)):
            day_file = params['run_name']+'/day_data/day_{}_{:02d}_{:02d}.h5'.format(day_start.year,
            day_start.month,day_start.day)
            day_data_exists = True

        elif os.path.exists(params['run_name']+'/day_data/day_{}_{:02d}_{:02d}.mseed'.format(day_start.year,
            day_start.month,day_start.day)):
            day_file = params['run_name']+'/day_data/day_{}_{:02d}_{:02d}.mseed'.format(day_start.year,
            day_start.month,day_start.day)
            day_data_exists = True

        elif os.path.exists(params['run_name']+'/day_data/day_{:03d}.mseed'.format(i_day)):
            day_data_exists = True
            day_file = params['run_name']+'/day_data/day_{:03d}.mseed'.format(i_day)
        else:
            day_data_exists = False

        if os.path.exists(params['run_name']+'/day_xcorrs/day_{}_{:02d}_{:02d}'.format(day_start.year,
            day_start.month,day_start.day)):
            day_xcorrs_exists = True
        elif os.path.exists(params['run_name']+'/day_xcorrs/day_{:03d}'.format(i_day)):
            day_xcorrs_exists = True
        else:
            day_xcorrs_exists = False

        if download and day_data_exists == False:
           
            st,inventory = get_waveforms(params, starttime=day_start,i_day=i_day,
                               seg_dur_s=seg_dur_s, remove_response=remove_response)

            #print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
            #print('coming from main--- result from get_waveforms')
            #for tr in st:
            #    print(tr)
            #print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')

            if st == None:
                print('Found no data for {} {:02d} {:02d}'.format(day_start.year,
                    day_start.month,day_start.day))
                continue
            else:
                print('main: got waveforms for day {}'.format(i_day))
                print(st)

                if params['format'].lower() == 'asdf':
                    ds = pyasdf.ASDFDataSet(params['run_name']+'/day_data/day_{}_{:02d}_{:02d}.h5'.format(day_start.year,day_start.month,day_start.day),mpi=False,mode='w')
                    for tr in st:
                        tag = '{}_{}_{}'.format(tr.stats.network.lower(),tr.stats.station.lower(),tr.stats.channel.lower())
                        ds.add_waveforms(tr,tag=tag)

                        inv_here = inventory.select(station=tr.stats.station)
                        ds.add_stationxml(inv_here)

                elif params['format'].lower() == 'miniseed' or params['format'].lower() == 'mseed':
                    st.write(params['run_name']+'/day_data/day_{}_{:02d}_{:02d}.mseed'.format(day_start.year,day_start.month,day_start.day))
                    inventory.write(params['run_name']+'/stations/stations_{}_{:02d}_{:02d}.xml'.format(day_start.year,day_start.month,day_start.day),format='STATIONXML')

                else: 
                    raise ValueError('format not recognized... use "miniseed" or "asdf"')
        #read in daily data if only running cross correlations
        day_data_dir = params['run_name']+'/day_data'
        day_stations_dir = params['run_name']+'/stations'

        if xcorr and not download:

            print("XCORR ONLY MODE")

            if params['format'].lower() == 'asdf':
                try:
                    st = obspy.Stream()
                    inventory = obspy.Inventory()
                    ds = pyasdf.ASDFDataSet(day_data_dir+'/day_{}_{:02d}_{:02d}.h5'.format(day_start.year,day_start.month,day_start.day),mpi=False,mode='r')

                    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
                    print(ds)
                    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

                    for waveform in ds.waveforms:
                        inv_ = waveform.StationXML
                        inventory += inv_

                        for net_ in inv_:
                            net_code = net_.code
                       
                            for sta_ in net_:
                                sta_code = sta_.code 

                                for chn_ in sta_:
                                    chn_code = chn_.code

                                    wave_tag = '{}_{}_{}'.format(net_code.lower(),
                                                                 sta_code.lower(),
                                                                 chn_code.lower()) 
                                    st += waveform[wave_tag]

                except:
                    print('skipping xcorrs for {}_{:02d}_{:02d}... no data found'.format(day_start.year,
                          day_start.month,day_start.day))
                    continue

            elif params['format'].lower() == 'miniseed' or params['format'].lower() == 'mseed':
                try:
                    print('trying to read mseed')
                    st = obspy.read(day_data_dir+'/day_{}_{:02d}_{:02d}.mseed'.format(day_start.year,
                        day_start.month,day_start.day))
                    inventory = obspy.read_inventory(day_stations_dir+'/stations_{}_{:02d}_{:02d}.xml'.format(day_start.year,
                                                     day_start.month,day_start.day))
                except IOError:
                    st = obspy.read(day_data_dir+'/day_{:03d}.mseed'.format(i_day))
                    inventory = obspy.read_inventory(day_stations_dir+'/stations_day_{:03d}.xml'.format(i_day))

            else:
                raise ValueError('format not known... use "asdf" or "miniseed"')

        #in case there are any BH1/BH2 channels, rotate to BHN,BHE

        #if params['rotate_to_zne'].lower() == 'true':
        rotate_to_zne = True
        if rotate_to_zne:
            try:
                st._rotate_to_zne(inventory)
            except:
                pass

        #For fairfield nodal data
        nodal = False
        if nodal:
            for tr in st:
                if tr.stats.channel == 'DP1':
	                tr.stats.channel = 'DPN'
                if tr.stats.channel == 'DP2':
	                tr.stats.channel = 'DPE'

        #run daily cross correlations
        if xcorr and len(st) >= 2 and day_xcorrs_exists == False:
            #check if day xcorrs exists
        
            print('RUNNING XCORRS FOR DAY', i_day)
            print('STREAM:')
            print(st)
            print('XXXXX')
            run_xcorrs(st,params,day_start,i_day)

        if i_day%size!=rank: continue

    #comm.barrier()
    if rank == 0:
        sys.exit()

if __name__ == "__main__":
    params_file = argv[1]
    if len(argv) == 2:
        day_start = int(argv[2])
    else:
        day_start = 0
    main(params_file,day_start)
