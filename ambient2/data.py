from __future__ import print_function
import obspy
import pyasdf
import numpy as np
import matplotlib.pyplot as plt

from configobj import ConfigObj
from obspy import UTCDateTime
from obspy import read_inventory
from obspy.clients.fdsn import Client
from obspy.signal.rotate import rotate2zne

def get_waveforms(params, starttime, seg_dur_s, i_day, remove_response=False, debug=True):

    # create empty stream and inventory objects
    st = obspy.Stream()
    inventory = obspy.Inventory()

    networks = params['network']
    location = params['location']
    channels = params['channels']
    # stations_dir = params['run_name']+'/stations'
    samprate = float(params['samprate'])
    nyquist = 0.5 * samprate

    # get data sources and passwords
    if type(params['data_source']) == list:
        data_sources = params['data_source']
    else:
        data_sources = [params['data_source']]

    if type(params['username']) == list:
        usernames = params['username']
    else:
        usernames = [params['username']]

    if type(params['password']) == list:
        passwords = params['password']
    else:
        passwords = [params['password']]

    # create list of clients:
    clients = []
    clients_sta = []

    for i, data_source in enumerate(data_sources):

        if passwords[i].lower() != "none" and data_source.lower() == 'iris':
            dataselect = 'http://service.iris.edu/ph5ws/dataselect/1'
            _station = 'http://service.iris.edu/ph5ws/station/1'
            clients.append(Client('http://service.iris.edu',
                           service_mappings={'dataselect': dataselect},
                           user=usernames[i],
                           password=passwords[i]))
            clients_sta.append(Client('http://service.iris.edu',
                           service_mappings={'station': _station},
                           user=usernames[i],
                           password=passwords[i], debug=True))

        elif passwords[i].lower() == "none" and params['dataselect'] == 'True':
            print('using dataselect without passwords')
            dataselect = 'http://service.iris.edu/ph5ws/dataselect/1'
            _station = 'http://service.iris.edu/ph5ws/station/1'
            clients.append(Client('http://service.iris.edu',
                           service_mappings={'dataselect': dataselect},
                           user=None,
                           password=None))
            clients_sta.append(Client('http://service.iris.edu',
                           service_mappings={'station': _station},
                           user=None,
                           password=None, debug=True))

        elif data_source.lower() == 'insight':

            from ambient2.ambient2 import msds_fdsn_client as mars_client
            clients.append(mars_client.Client('https://ws.seis-insight.eu',
                                              user=usernames[i],
                                              password=passwords[i],
                                              debug=True))
            clients_sta.append(mars_client.Client('https://ws.seis-insight.eu',
                                              user=usernames[i],
                                              password=passwords[i],
                                              debug=True))
        else:
            clients.append(Client(data_source))
            clients_sta.append(Client(data_source))

    # save daily data?
    if params['save_day_data'] == 'True':
        save_day_data = True
    elif params['save_day_data'] == 'False':
        save_day_data = False

    # make sure stations request makes sense
    # still problems with a single station?
    if params['stations'] == 'ALL':
        stations = '*'
    else:
        stations = params['stations']
        if len(stations) > 1:
            stations = ",".join(stations)
        elif len(stations) == 1:
            stations = stations
        elif len(stations) < 1:
            raise ValueError('NO STATIONS FOUND')

    t1 = UTCDateTime(starttime)
    t2 = starttime + seg_dur_s + 1.0  # try not to get too few samples
    # t2 = starttime + seg_dur_s #try not to get too few samples
    s_per_day = 86400.0

    if type(channels) == list:
        channels = ','.join(channels)

    if type(networks) != list:
        networks = [networks]

    if params['use_box'] == 'True':
        minlongitude = params['min_lon']
        maxlongitude = params['max_lon']
        minlatitude = params['min_lat']
        maxlatitude = params['max_lat']
    else:
        minlongitude = None
        maxlongitude = None
        minlatitude = None
        maxlatitude = None

    # loop through networks and try to append to inventory
    for network in networks:
        for client_sta in clients_sta:

            try:
                inventory += client_sta.get_stations(network=network,
                                                     station=stations,
                                                     starttime=t1,
                                                     endtime=t2,
                                                     channel=channels,
                                                     location=location,
                                                     level='response',
                                                     minlongitude=minlongitude,
                                                     maxlongitude=maxlongitude,
                                                     minlatitude=minlatitude,
                                                     maxlatitude=maxlatitude)
            except:
                pass

    use_bulk = False
    #use_bulk = True

    # download with bulk (assumes only one data source)
    if use_bulk:
        client = clients[0]
        bulk = []

        for net_here in inventory:
            stations = net_here.stations
            for i,station in enumerate(stations):
                bulk.append( (net_here.code, station.code, '*', channels, t1, t2) )

        st = client.get_waveforms_bulk(bulk, attach_response=True, minimumlength=None,longestonly=None)
        print('requested {} waveforms... client.get_waveforms recevied {}'.format(len(bulk)*3,len(st)))

        #return nothing if client.get_waveforms_bulk doesn't find anything
        if len(st) == 0:
            return None,None

        else:
            # enforce strict starttime
            for tr in st:
                tr.stats.starttime = starttime

            r1 = 50.0
            r = r1/((1./samprate)*0.5*len(st[0].data))
            st.taper(r)
            st.detrend()
            st.detrend('demean')

            # get rid of short streams
            reject_list = []
            for i,tr in enumerate(st):
                if (tr.stats.endtime - tr.stats.starttime) < s_per_day:
                    reject_list.append(tr.stats.station)

            for tr in st:
                if tr.stats.station in reject_list:
                    st.remove(tr)

            npts_req = int(seg_dur_s*samprate)

            #in case all traces got removed by the filter
            if len(st) == 0:
                print('all traces were removed!')
                return None,None

            else:

                # interpolate to standard time axis
                if samprate != st[0].stats.sampling_rate:
                    st.filter('lowpass',freq=nyquist*0.5,corners=4,zerophase=True) #lowpass anti-alias filter
                print("interpolating to new sampling rate: {} Hz".format(samprate))
                st.interpolate(sampling_rate=samprate,
                           starttime=starttime,
                           method='nearest',
                           npts=npts_req)

    # don't download in bulk
    else:

        for network in networks:
            for client in clients:
                try:
                    net_here = inventory.select(network)[0]
                    stations = net_here.stations
                except:
                    continue

                for i,station in enumerate(stations):
                    print('-------------------------------------------------------------')
                    print('querying client for {} {}'.format(net_here.code, station.code))
                    print('-------------------------------------------------------------')
                    try:
                        st_here = client.get_waveforms(net_here.code, station.code, location, channels, t1, t2)
                    except: 
                        print('got no waveforms from client {} for day {}'.format(client,i_day))
                        continue

                    print(st_here)

                    #check if more than one channel (sampling rate) exists, and take the lower sampling rate
                    sps = []
                    for tr in st_here:
                        sps.append(tr.stats.sampling_rate)
                    st_here = st_here.select(sampling_rate=np.min(sps))

                    if len(st_here) == 0:
                        continue

                    else:
                        st_here.merge(method=1,fill_value=0) #attempt at gap management

                        # get rid of short streams
                        reject_list = []
                        npts_req_here = st_here[0].stats.sampling_rate * s_per_day
                        for i,tr in enumerate(st_here):
                            if tr.stats.npts < npts_req_here:
                                print("the length of the trace was {}, but {} were required".format(tr.stats.npts,npts_req_here))
                                reject_list.append(tr.stats.station)

                        for tr in st_here:
                            if tr.stats.station in reject_list:
                                st_here.remove(tr)

                        #check if traces are still in stream
                        if len(st_here) == 0:
                            continue

                        else:
                            # taper
                            r1 = 50.0
                            r = r1/((1./samprate)*0.5*len(st_here[0].data))
                            st_here.taper(r)
                            st_here.detrend()
                            st_here.detrend('demean')

                            # enforce strict starttime
                            for tr in st_here:
                                tr.stats.starttime = starttime

                            # interpolate to standard time axis at requested samprate
                            if samprate != st_here[0].stats.sampling_rate:
                                st_here.filter('lowpass',freq=nyquist*0.5,corners=4,zerophase=True) #lowpass anti-alias filter
                            npts_req = int(seg_dur_s*samprate)
                            print("interpolating to new sampling rate: {} Hz".format(samprate))
                            st_here.interpolate(sampling_rate=samprate,
                                        starttime=starttime,
                                        method='nearest',
                                        npts=npts_req)

                            st += st_here

    if len(st) == 0:
        return None,None

    else:

        st.merge(method=1) #in case of doubles
        print('####################################################')
        print('####################################################')
        print('####################################################')
        print(st)
        print('####################################################')
        print('####################################################')
        print('####################################################')

        if remove_response:

            if len(data_sources) == 1 and data_source == 'insight':
                pre_filt = [0.005,0.01,8.,10.]
                st.detrend('demean')
                st.detrend()
                st.remove_response(inventory=inventory,
                                   pre_filt=pre_filt,
                                   output='VEL',
                                   taper_fraction=0.05)
                # rotate from U,V,W to Z,N,E
                uvw_chnls = ['BHU','BHV','BHW'] 
                zne_chnls = ['BHZ','BHN','BHE']
                sta = inventory[0][0]
                azs = [] 
                dips = []
                trs = []

                for chn in uvw_chnls:
                    chndata = sta.select(channel=chn)[0]
                    azs.append(chndata.azimuth)
                    dips.append(chndata.dip)
                    trs.append(st.select(channel=chn)[0])

                (z, n, e) = rotate2zne(trs[0], azs[0], dips[0],
                                       trs[1], azs[1], dips[1],
                                       trs[2], azs[2], dips[2])
                datar = [z, n, e]
                for j, tr_ in enumerate(st):
                    tr_.data = datar[j]
                    tr_.stats.channel = zne_chnls[j]
                                      
            else:
                pre_filt = [0.001, 0.005, 45, 50] #might want to play around with this a bit
                st.remove_response(inventory=inventory,
                                   pre_filt=pre_filt,
                                   output='VEL',
                                   water_level=20.)

                # for i,tr in enumerate(st):
                #    tr.remove_response(inventory=inventory, output='VEL', water_level=20, pre_filt=pre_filt)

        # add in station location information from the inventory
        for tr in st:
            for network in inventory:
                for station in network:
                    if tr.stats.station == station.code:
                        tr.stats.longitude = station.longitude
                        tr.stats.latitude = station.latitude
                        tr.stats.elevation = station.elevation

        print('made it this far... returning st {} and inv {}'.format(st,inventory))
        return st,inventory
