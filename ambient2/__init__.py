import sys
if sys.version_info[0] < 3:

    import data
    import utils
    import main
else:
    #from ambient2.ambient2 import data
    #from ambient2.ambient2 import utils
    #from ambient2.ambient2 import main
    from ambient2 import data
    from ambient2 import utils
    from ambient2 import main
