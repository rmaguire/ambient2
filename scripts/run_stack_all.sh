#!/bin/bash

#write master paths list
ambient2_write_paths_list

mkdir sac_xcorrs

#loop over all paths
for path in `cat master_paths.txt`; 
do
    echo $path
    
    #zz cross correlations
    mkdir sac_xcorrs/ZZ
    ls -1d day_xcorrs/*/ZZ/$path/* > filelist.txt
    ts_pws filelist.txt osac=$path"_ZZ" verbose

    #nn cross correlations
    mkdir sac_xcorrs/NN
    ls -1d day_xcorrs/*/NN/$path/* > filelist.txt
    ts_pws filelist.txt osac=$path"_NN" verbose

    #ee cross correlations
    mkdir sac_xcorrs/EE
    ls -1d day_xcorrs/*/EE/$path/* > filelist.txt
    ts_pws filelist.txt osac=$path"_EE" verbose

    #ZN cross correlations
    mkdir sac_xcorrs/ZN
    ls -1d day_xcorrs/*/ZN/$path/* > filelist.txt
    ts_pws filelist.txt osac=$path"_ZN" verbose

    #NZ cross correlations
    mkdir sac_xcorrs/NZ
    ls -1d day_xcorrs/*/NZ/$path/* > filelist.txt
    ts_pws filelist.txt osac=$path"_NZ" verbose

    #ZE cross correlations
    mkdir sac_xcorrs/ZE
    ls -1d day_xcorrs/*/ZE/$path/* > filelist.txt
    ts_pws filelist.txt osac=$path"_ZE" verbose

    #EZ cross correlations
    mkdir sac_xcorrs/EZ
    ls -1d day_xcorrs/*/EZ/$path/* > filelist.txt
    ts_pws filelist.txt osac=$path"_EZ" verbose

    #NE cross correlations
    mkdir sac_xcorrs/NE
    ls -1d day_xcorrs/*/NE/$path/* > filelist.txt
    ts_pws filelist.txt osac=$path"_NE" verbose

    #EN cross correlations
    mkdir sac_xcorrs/EN
    ls -1d day_xcorrs/*/EN/$path/* > filelist.txt
    ts_pws filelist.txt osac=$path"_EN" verbose

done

mv *_ZZ* sac_xcorrs/ZZ
mv *_NN* sac_xcorrs/NN
mv *_EE* sac_xcorrs/EE
mv *_ZN* sac_xcorrs/ZN
mv *_NZ* sac_xcorrs/NZ
mv *_ZE* sac_xcorrs/ZE
mv *_EZ* sac_xcorrs/EZ
mv *_NE* sac_xcorrs/NE
mv *_EN* sac_xcorrs/EN
