#!/bin/bash

#write master paths list
ambient2_write_paths_list

mkdir sac_xcorrs

#loop over all paths
for path in `cat master_paths.txt`; 
do
    echo $path
    
    #zz cross correlations
    mkdir sac_xcorrs/ZZ
    ls -1d day_xcorrs/*/ZZ/$path/* > filelist.txt
    ts_pws filelist.txt osac=$path"_ZZ" verbose

done

mv *_ZZ* sac_xcorrs/ZZ
