#!/usr/bin/env python
from __future__ import absolute_import
import re
import os
import numpy as np
from os.path import join
#from numpy.distutils.core import setup,Extension
from setuptools import find_packages
from setuptools import setup, Extension

pwd=os.getcwd()

setup(name='ambient2',
      version='0.1',
      description='Tools for processing and correlating ambient noise',
      author='Ross Maguire',
      author_email='rmaguire@unm.edu',
      url='www.gitlab.com/rmaguire/ambient2',
      #packages=find_packages(),
      packages=['ambient2'],
      scripts=['scripts/ambient2_run','scripts/ambient2_write_paths_list',
               'scripts/ambient2_writesac','scripts/ambient2_stack',
               'scripts/ambient2_rotate','scripts/ambient2_rotate_TT'],
      license='None')
